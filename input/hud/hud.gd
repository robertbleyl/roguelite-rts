extends CanvasLayer

class_name HUD

@onready var mini_map: MiniMap = %MiniMap

signal move_command_issued(position: Vector2, attack_move: bool)

func _on_mini_map_move_command_issued(position: Vector2, attack_move: bool):
	move_command_issued.emit(position, attack_move)

func _on_level_area_body_entered(body: Node2D):
	mini_map.node_entered_level(body)

func _on_level_area_body_exited(body: Node2D):
	mini_map.node_exited_level(body)

func _on_level_area_area_shape_entered(_area_rid, area: Area2D, _area_shape_index, _local_shape_index):
	mini_map.node_entered_level(area)

func _on_level_area_area_shape_exited(_area_rid, area: Area2D, _area_shape_index, _local_shape_index):
	mini_map.node_exited_level(area)

func level_area_initialized(shape: CollisionShape2D):
	mini_map.level_area_initialized(shape)
