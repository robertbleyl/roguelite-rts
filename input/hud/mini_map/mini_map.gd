extends Node2D

class_name MiniMap

@onready var container: Container = %Container
@onready var panel: Panel = %Panel

@export var mini_map_height_percentage: float = 0.2

signal move_command_issued(position: Vector2, attack_move: bool)

var _mini_map_items: Array[Node2D] = []

var _world_top_left: Vector2
var _world_size: float
var _mini_map_to_world_ratio: float

func level_area_initialized(collision_shape: CollisionShape2D):
	var rect_shape: RectangleShape2D = collision_shape.shape
	_world_size = rect_shape.size.length()
	_world_top_left = collision_shape.global_position - rect_shape.size / 2
	
	var mini_map_height = get_viewport_rect().size.y * mini_map_height_percentage
	container.size.y = mini_map_height
	var rect_shape_ratio = rect_shape.size.y / rect_shape.size.x
	container.size.x = mini_map_height / rect_shape_ratio
	container.position.y = get_viewport_rect().size.y - mini_map_height
	
	_mini_map_to_world_ratio = container.size.length() / _world_size
	
func _process(_delta: float):
	for item: Node2D in _mini_map_items:
		if item != null:
			item.get_mini_map_icon().position = _global_to_mini_map_pos(item.global_position)

func _global_to_mini_map_pos(global_pos: Vector2) -> Vector2:
	var world_top_left_dir = global_pos - _world_top_left
	return world_top_left_dir * _mini_map_to_world_ratio

func _mini_map_to_global_pos(mini_map_pos: Vector2) -> Vector2:
	return _world_top_left + mini_map_pos / _mini_map_to_world_ratio

func node_entered_level(body: Node2D):
	if body != null && body.has_method("get_mini_map_icon"):
		var icon = body.get_mini_map_icon()
		panel.add_child(icon)
		_mini_map_items.push_back(body)
		
func node_exited_level(body: Node2D):
	if body != null && body.has_method("get_mini_map_icon"):
		panel.remove_child(body.get_mini_map_icon())
		_mini_map_items.erase(body)

func _gui_input(event: InputEvent):
	if Input.is_action_just_pressed('Select Units'):
		_move_camera_to_pos(event.position)
	elif Input.is_action_just_pressed('Unit Move Command'):
		_move_command_to_pos(event.position, false)
	elif Input.is_action_just_pressed('Unit Attack Command'):
		_move_command_to_pos(panel.get_local_mouse_position(), true)
		panel.accept_event()

func _move_camera_to_pos(mini_map_pos: Vector2):
	var global_pos = _mini_map_to_global_pos(mini_map_pos)
	get_viewport().get_camera_2d().global_position = global_pos

func _move_command_to_pos(mini_map_pos: Vector2, attack_move: bool):
	var global_pos = _mini_map_to_global_pos(mini_map_pos)
	move_command_issued.emit(global_pos, attack_move)

func _on_panel_mouse_entered():
	panel.grab_focus()

func _on_panel_mouse_exited():
	panel.release_focus()
