extends Area2D

@onready var hud: HUD = %HUD
@onready var shape: CollisionShape2D = %CollisionShape2D

func _ready():
	hud.level_area_initialized.call_deferred(shape)
