extends Node2D

@export var double_tap_delay: float = 0.25

signal control_group_selected(seleted_units: Dictionary)

var _units_without_group: Dictionary = {}
var _selected_units: Dictionary

var _groups: Dictionary = {}

var _double_tap_timer: float
var _previous_action_key: String

func _unhandled_key_input(_event: InputEvent):
	if Input.is_action_just_pressed("Select All Units Without Control Group"):
		control_group_selected.emit(_units_without_group)
		_check_camera_jump("Select All Units Without Control Group", _units_without_group)
		return
	
	for i in range(1, 10):
		if Input.is_action_just_pressed("Control Group %d" % i):
			if Input.is_action_pressed("Control Group Steal Modifier"):
				_steal_to_group(i)
			else:
				_select_group(i)
			break;

func _steal_to_group(group: int):
	for unit: Unit in _selected_units.values():
		if unit.group > 0:
			var units_of_current_group: Dictionary = _groups[unit.group]
			units_of_current_group.erase(unit.get_instance_id())
		else:
			_units_without_group.erase(unit.get_instance_id())
		
		unit.group = group
		
		if !_groups.has(group):
			_groups[group] = {}
		
		var units_of_new_group: Dictionary = _groups[group]
		units_of_new_group[unit.get_instance_id()] = unit

func _select_group(group: int):
	if _groups.has(group):
		var units = _groups[group]
		control_group_selected.emit(units)
		_check_camera_jump("Control Group %d" % group, units)

func _check_camera_jump(action_key: String, units: Dictionary):
	if _previous_action_key == action_key && Input.is_action_pressed(action_key) && _double_tap_timer >= double_tap_delay:
		var center_pos = Vector2.ZERO
		
		for unit: Unit in units.values():
			center_pos += unit.global_position
		
		center_pos /= units.size()
		
		get_viewport().get_camera_2d().global_position = center_pos
	
	_previous_action_key = action_key

func _process(delta: float):
	_double_tap_timer += delta

func _on_unit_selection_unit_selection_changed(selected_units: Dictionary):
	_selected_units = selected_units

func _on_level_area_body_entered(body: Node2D):
	if body != null && body is Unit && body.is_player_unit():
		_units_without_group[body.get_instance_id()] = body
		body.tree_exiting.connect(_remove_dead_unit.bind(body))

func _remove_dead_unit(unit: Unit):
	if unit.group > 0:
		var group: Dictionary = _groups[unit.group];
		group.erase(unit.get_instance_id())
		
		if group.is_empty():
			_groups.erase(unit.group)
	else:
		_units_without_group.erase(unit.get_instance_id())
