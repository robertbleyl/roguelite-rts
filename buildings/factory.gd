extends Area2D
class_name Factory

@export var initial_capture_status: CaptureStatus = CaptureStatus.NEUTRAL
@export var respawn_count_down: float = 20.0
@export var respawn_count: int = 20
@export var player_unit_to_spawn: PackedScene = preload("res://units/base_units/base_unit.tscn")
@export var enemy_unit_to_spawn: PackedScene = preload("res://units/base_units/base_enemy.tscn")
@export var mini_map_icon_texture: Texture2D

@onready var capture_area: CollisionShape2D = %CaptureArea

var _player_unit_count = 0
var _enemy_unit_count = 0

var _capture_status: CaptureStatus
var _respawn_timer: float

var _radius: float

var _mini_map_icon: Control

func _ready():
	_capture_status = initial_capture_status
	
	var circle: CircleShape2D = capture_area.shape
	_radius = circle.radius
	
	_mini_map_icon = TextureRect.new()
	_mini_map_icon.texture = mini_map_icon_texture
	_mini_map_icon.size = Vector2.ONE * 8
	_mini_map_icon.scale = Vector2.ONE * 0.5

func _process(delta: float):
	if _capture_status == CaptureStatus.NEUTRAL:
		return
	
	_respawn_timer += delta;
	
	if _respawn_timer < respawn_count_down:
		return
	
	if _capture_status == CaptureStatus.PLAYER:
		_spawn_units(player_unit_to_spawn)
	elif _capture_status == CaptureStatus.ENEMY:
		_spawn_units(enemy_unit_to_spawn)
	
	_respawn_timer = 0

func _on_body_entered(body: Unit):
	if _is_player_unit(body):
		_player_unit_count += 1
	elif _is_enemy_unit(body):
		_enemy_unit_count += 1
	
	_check_capture_state();

func _on_body_exited(body: Unit):
	if _is_player_unit(body):
		_player_unit_count -= 1
	elif _is_enemy_unit(body):
		_enemy_unit_count -= 1
	
	_check_capture_state()
	
func _is_player_unit(body: Node2D) -> bool:
	return _matches_layer(body, 2)
	
func _is_enemy_unit(body: Node2D) -> bool:
	return _matches_layer(body, 4)

func _matches_layer(body: Node2D, bit: int) -> bool:
	if !body is Unit:
		return false
	
	var unit: Unit = body
	return unit.collision_layer & bit == bit

func _check_capture_state():
	if _enemy_unit_count == 0 && _player_unit_count > 0:
		_capture_status = CaptureStatus.PLAYER
	elif _enemy_unit_count > 0 && _player_unit_count == 0:
		_capture_status = CaptureStatus.ENEMY

func _spawn_units(unit_to_spawn: PackedScene):
	for n in respawn_count:
		var unit = unit_to_spawn.instantiate()
		var direction = Vector2.UP.rotated(randf() * PI * 2)
		get_parent().add_child(unit)
		unit.global_position = global_position + direction * _radius

func get_mini_map_icon() -> Control:
	return _mini_map_icon

enum CaptureStatus {
	PLAYER,
	NEUTRAL,
	ENEMY
}
